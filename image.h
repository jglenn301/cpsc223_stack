#ifndef __IMAGE_H__
#define __IMAGE_H__

#include <stdio.h>

typedef struct _image image;

/**
 * Creates an image with the given dimensions, with all pixels initialized
 * to zero.  Returns NULL if there is a memory allocation error.
 * It is the caller's responsibility to destroy the image.
 *
 * @param height a positive integer
 * @param width a positive integer
 * @return a pointer to an image, or NULL
 */
image *image_create(size_t height, size_t width);

/**
 * Fills the given rectangle in the given image with the given value.
 *
 * @param img a pointer to an image, non-NULL
 * @param r a row index in img
 * @param c a column index in img
 * @param h a positive integer so that r + h <= the height of img
 * @param w a positive integer so that c + w <= the width of img
 * @param ch a character
 */
void image_fill_rect(image *img, size_t r, size_t c, size_t h, size_t w, char ch);

/**
 * Returns the size of the blob that contains the given coordinate.
 *
 * @param img a pointer to an image, non-NULL
 * @param r a row index in img
 * @param c a column index in img
 * @return a positive integer
 */
size_t image_blob_size(image *img, size_t r, size_t c);

/**
 * Prints the given image to the given file.
 *
 * @param out an output file
 * @param img a pointer to an image, non-NULL
 */
void image_fprint(FILE *out, const image *img);

/**
 * Destroys the given image, releasing all the resources it holds.
 *
 * @param img a pointer to an image, non-NULL
 */
void image_destroy(image *img);

#endif
