#include <stdio.h>
#include <stdlib.h>

#include "image.h"

int main(int argc, char **argv)
{
  if (argc < 5)
    {
      fprintf(stderr, "USAGE: %s height width [fillr fillc fillw fillc fillc]... r c\n", argv[0]);
      return 1;
    }

  if (atol(argv[1]) <= 0 || atol(argv[2]) <= 0)
    {
      fprintf(stderr, "%s: dimensions must be positive\n", argv[0]);
    }

  size_t h = atol(argv[1]);
  size_t w = atol(argv[2]);

  image *img = image_create(h, w);
  if (img == NULL)
    {
      fprintf(stderr, "%s: could not create image\n", argv[0]);
      return 1;
    }
  image_fill_rect(img, 0, 0, h, w, '.');

  size_t a = 3;
  while (a + 5 <= argc)
    {
      if (atol(argv[a]) < 0 || atol(argv[a + 1]) < 0)
	{
	  fprintf(stderr, "%s: rectangle coordinates must be nonnegative\n", argv[0]);
	  image_destroy(img);
	  return 1;
	}
	
      if (atol(argv[a + 2]) <= 0 || atol(argv[a + 3]) <= 0)
	{
	  fprintf(stderr, "%s: rectangle coordinates must be positive\n", argv[0]);
	  image_destroy(img);
	  return 1;
	}

      image_fill_rect(img, atol(argv[a]), atol(argv[a + 1]), atol(argv[a + 2]), atol(argv[a + 3]), argv[a + 4][0]);
      
      a += 5;
    }

  if (a + 2 <= argc)
    {
      if (atol(argv[a]) < 0 || atol(argv[a + 1]) < 0)
	{
	  fprintf(stderr, "%s: blob coordinates must be nonnegative\n", argv[0]);
	  image_destroy(img);
	  return 1;
	}

      printf("%lu\n", image_blob_size(img, atol(argv[a]), atol(argv[a + 1])));
      a += 2;
    }

  if (a < argc && argv[a][0] == 'p')
    {
      image_fprint(stdout, img);
    }
  
  
  image_destroy(img);
}
