An example of replacing recursion with a stack.  This uses an array based
version of a stack.  Run with

```
./Blob 10 10 1 1 2 3 '@' 3 3 1 4 '@' 4 6 2 2 '@' 4 1 1 3 '@' 2 2 p
```

where the command-line arguments allow you to draw in the space and calculate
the size of the blob that contains a given point.  The parameters are:

1. the height and width of the canvas to draw in
2. sets of five parameters giving the upper left corners of rectangles to
   draw, the height and width, and the "color" (character) to draw them in
3. the row and column of the point for which to find the size of the
   containing blob
4. p to print the resulting canvas

The arguments above result in
```
17
..........
.@@@......
.@@@......
...@@@@...
.@@@..@@..
......@@..
..........
..........
..........
..........
```