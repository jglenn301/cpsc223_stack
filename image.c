#include <stdlib.h>

#include "image.h"
#include "list.h"

struct _image
{
  char **pixels;
  size_t height;
  size_t width;
};

size_t image_blob_helper(image *img, size_t r, size_t c, char color, list *visited);

image *image_create(size_t h, size_t w)
{
  image *result = malloc(sizeof(image));
  if (result != NULL)
    {
      result->height = h;
      result->width = w;

      result->pixels = malloc(sizeof(char *) * h);
      if (result->pixels == NULL)
	{
	  free(result);
	  return NULL;
	}
      
      for (size_t r = 0; r < h; r++)
	{
	  result->pixels[r] = calloc(w, sizeof(char));
	  if (result->pixels[r] == NULL)
	    {
	      // make the result well-formed enough for image_destroy to work
	      result->height = r - 1;
	      image_destroy(result);
	      return NULL;
	    }
	}
    }

  return result;
}

void image_fill_rect(image *img, size_t r, size_t c, size_t h, size_t w, char ch)
{
  for (size_t fill_r = r; fill_r < r + h && fill_r < img->height; fill_r++)
    {
      for (size_t fill_c = c; fill_c < c + w && fill_c < img->width; fill_c++)
	{
	  img->pixels[fill_r][fill_c] = ch;
	}
    }
}

size_t image_blob_size(image *img, size_t r, size_t c)
{
  char color = img->pixels[r][c];
  list *visited = list_create();
  if (visited == NULL)
    {
      return 0;
    }

  size_t count = image_blob_helper(img, r, c, color, visited);

  // restore pixels to original color
  size_t size = list_size(visited);
  for (size_t i = 0; i < size; i++)
    {
      list_element coord;
      list_get(visited, i, &coord);
      img->pixels[coord.row][coord.col] = color;
    }

  list_destroy(visited);

  return count;
}

size_t image_blob_helper(image *img, size_t r, size_t c, char color, list *visited)
{
  size_t count = 0;
  if (img->pixels[r][c] == color)
    {
      // mark pixel as visited and record
      img->pixels[r][c] = color + 1; // overflow to 0 will be OK
      list_element coord = {r, c};
      list_add_end(visited, &coord);

      count = 1;
      // recursively visit meighbors
      if (r > 0)
	{
	  count += image_blob_helper(img, r - 1, c, color, visited);
	}
      if (r < img->height - 1)
	{
	  count += image_blob_helper(img, r + 1, c, color, visited);
	}
      if (c > 0)
	{
	  count += image_blob_helper(img, r, c - 1, color, visited);
	}
      if (c < img->width - 1)
	{
	  count += image_blob_helper(img, r, c + 1 , color, visited);
	}
    }

  return count;
}

void image_fprint(FILE *out, const image *img)
{
  for (size_t r = 0; r < img->height; r++)
    {
      for (size_t c = 0; c < img->width; c++)
	{
	  fprintf(out, "%c", img->pixels[r][c]);
	}
      fprintf(out, "\n");
    }
}

void image_destroy(image *img)
{
  for (size_t r_to_free = 0; r_to_free < img->height; r_to_free++)
    {
      free(img->pixels[r_to_free]);
    }
  free(img->pixels);
  free(img);
}
