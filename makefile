CC=gcc
CFLAGS=-Wall -pedantic -std=c99 -g3

Blob: blob_test.o array_list.o image.o
	${CC} ${CCFLAGS} -o $@ $^ -lm

array_list.o: list.h
image.o: image.h
blob_test.o: list.h image.h
