#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include "list.h"

struct _list
{
  list_element *elements;  // pointer to the array containing the elements
  size_t size;             // the number of elements actually in the list
  size_t capacity;         // the max elements the current array can hold
};

#define LIST_INITIAL_CAPACITY (2)
#define LIST_MINIMUM_CAPACITY (2)

list *list_create()
{
  // make space for the meta-data struct
  list *result = malloc(sizeof(list));

  // make an array for future elements
  result->elements = malloc(sizeof(list_element) * LIST_INITIAL_CAPACITY);

  // initialize size and capacity
  result->size = 0;
  result->capacity = result->elements != NULL ? LIST_INITIAL_CAPACITY : 0;

  return result;
}

size_t list_size(const list *l)
{
      return l->size;
}

void list_get(list *l, size_t i, list_element *e)
{
  *e = l->elements[i];
}

bool list_add_end(list *l, const list_element *to_add)
{
  return list_add_at(l, to_add, l->size);
}

bool list_add_at(list *l, const list_element *to_add, size_t i)
{
  // make sure there's enough space
  if (l->capacity > 0 && l->size == l->capacity)
    {
      list_element *bigger = realloc(l->elements, sizeof(list_element) * l->capacity * 2);
      if (bigger != NULL)
	{
	  l->elements = bigger;
	  l->capacity *= 2;
	}
    }
  
  if (l->size < l->capacity)
    {
      // copy elements to higher index to make room
      for (size_t copy_to = l->size; copy_to > i; copy_to--)
	{
	  l->elements[copy_to] = l->elements[copy_to - 1];
	}
      l->elements[i] = *to_add;
      l->size++;
      return true;
    }
  else
    {
      return false;
    }
}

void list_remove_at(list *l, size_t i, list_element *e)
{
  // remember old element to return later
  if (e != NULL)
    {
      *e = l->elements[i];
    }
  
  // copy elements back to fill the hole
  for (size_t copy_to = i; copy_to < l->size - 1; copy_to++)
    {
      l->elements[copy_to] = l->elements[copy_to + 1];
    }

  l->size--;

  // resize if too much capacity
  if (l->size > l->capacity / 4 && l->capacity > LIST_MINIMUM_CAPACITY)
    {
      size_t new_cap = l->size > LIST_MINIMUM_CAPACITY ? l->size : LIST_MINIMUM_CAPACITY;
      list_element *smaller = realloc(l->elements, sizeof(list_element) * new_cap);
      if (smaller != NULL)
	{
	  // shouldn't fail, but the standard doesn't guarantee it won't
	  l->elements = smaller;
	  l->capacity = new_cap;
	}
    }
}

void list_destroy(list *l)
{
  // free the array
  free(l->elements);

  // free the meta-data struct
  free(l);
}
