#ifndef __LIST_H__
#define __LIST_H__

#include <stdlib.h>
#include <stdbool.h>

/**
 * The type of the items in the list.  We use this here so we can
 * easily change the type of the list, although with this setup we
 * can still only have one type of list per program.  Since elements
 * are copied into the various functions, it is recommended to only
 * use this with primitive and pointer types.  And for pointers, 
 * note that the list does not take ownership of the pointers added to
 * it, so freeing anything they point to remains the responsibility
 * of the user of the list.
 */
typedef struct
{
  size_t row;
  size_t col;
} list_element;

typedef struct _list list;

/**
 * Creates an empty list.  Returns NULL if there is a memory allocation
 * failure.  It is the caller's responsibility to eventually destroy
 * the list if it is non-NULL.
 *
 * @return a pointer to a list, or NULL
 */
list *list_create();

/**
 * Returns the size of the given list.
 *
 * @param l a pointer to a list, non-NULL
 */
size_t list_size(const list *l);

/**
 * Returns the element at the given location in this list.
 *
 * @param l a pointer to a list, non-NULL
 * @param i an index into that list
 * @param e a pointer to space for an element
 */
void list_get(list *l, size_t i, list_element *e);

/**
 * Adds the given element to the end of the given list.  Returns true
 * if the element was added and false if not (if there was a memory allocation
 * error).
 *
 * @param l a pointer to a list, non-NULL
 * @param to_add an element
 * @return a boolean
 */
bool list_add_end(list *l, const list_element *to_add);

/**
 * Adds the given elements at the given position in the given list.
 * All existing elements at or after that index are moved one spot
 * later (to higher indices) to make room for the new element.
 *
 * @param l a pointer to a list, non-NULL
 * @param to_add an element
 * @param i an index into the list
 * @return a boolean
 */
bool list_add_at(list *l, const list_element *to_add, size_t i);

/**
 * Removes the element from the given location in the given list.  All
 * elements after that index are moved one spot earlier (to lower
 * indices) to fill the space left by the removed element.  The removed
 * element is copied to the given location if the location is non-NULL.
 *
 * @param l a pointer to a list, non-NULL
 * @param i an index into the list
 * @param e a pointer to space for an element, or NULL
 */
void list_remove_at(list *l, size_t i, list_element *e);

/**
 * Destroys the given list.
 *
 * @param l a pointer to a list, non-NULL
 */
void list_destroy(list *l);

#endif
